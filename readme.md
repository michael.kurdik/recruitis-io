### Abstract

This application is based on description below - [Zadání](#zadání).

The assignment is simplified; however, some aspects are introduced perhaps somewhat artificially, i.e., in the real world, they may or may not be considered. Because the assignment is quite open in terms of architecture, it is mainly approached for the purpose of demonstrating how one can think about the problem.

This can also be said about the frontend, which is not processed via the Vue framework mainly for time reasons. It may be added in the future. 

The same applies to a bootrap that is loaded from a CDN or a template that includes a JS or CSS snippets. The whole thing should be moved to an external file, ideally through a build tool such as webpack resp. encore. etc.

### Application

I prefer cleaned laptop, so everything is moved into docker container. I can run it in same wave.

1.
```
docker run -it --rm --name php-apache-container -p 8080:80 -v $(pwd):/var/www/html  php:8.1-apache
```

2.
```
docker exec -it php-apache-container bash
```

3.
```
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
```

4.
```
apt install zip
apt install unzip
```

5.
```
composer --version
```

Application is running on http://localhost:8080/public/. 

Note. URL is just for demonstration , so don't care about *public* part :)

---

# Zadání

## Úkolem
Úkolem je napsat řešení, které bude stahovat pomocí našeho API aktuální vystavené inzeráty. Inzeráty získané přes API vypište na jednoduché stránce. V tomto úkolu nejde o grafiku, můžete použít základní grafický skeleton (Bootstrap, Bootflat, Materialize..) bez dalších úprav.

## Token pro napojení na naše API
```
89d985c4b1c25c26fe3b1595b4ef3137a0ebb549.11169.dd37716503850db285a143eeef3dd663
```

## Co musí projekt obsahovat
- Musí být napsán v jazyce PHP, verzi 8.1
- Musí obsahovat Symfony framework
- Práci s Recruitis API

## Co by měl obsahovat (seřazeno dle důležitosti)
1. Čistý a okomentovaný kód
2. Cachování výsledků z našeho API
3. Otestování alespoň jedné třídy
4. Ošetření výjimek a nefunkčnosti API
5. Řešení stránkování výsledku API
6. (Volitelné) Vue komponenta pro výpis inzerátů – Vytvoření jednoduchého výpisu inzerátu pomocí Vue komponenty, která si bude načítat data z Vaší Symfony aplikace. Tento bod není povinný.

## Na co se budeme zaměřovat
- Čitelnost backend kódu
- Kompletní přístup k danému úkolu – Jak si autor poradil s jednotlivými kroky.

## Poznámky
- Můžete využívat již existující knihovny od Symfony, popř. jiného vendora (např. Guzzle). To, jak k úkolu přistoupíte, je zcela na Vás. Hodnotíme efektivitu a čitelnost kódu. Na grafice nezáleží, není potřeba ji věnovat extra času.
- Odevzdání projektu může být přes GitHub (či jiný repozitář), Docker image, popř. přes online uložiště.
- Projekt může být vystaven i funkční online na URL adrese (není podmínkou).