<?php declare(strict_types=1);

namespace App\Tests\Service;

use App\Factory\AddressFactory;
use App\Factory\ContactFactory;
use App\Factory\JobFactory;
use App\Factory\SalaryFactory;
use App\Service\ApiClient;
use App\Service\JobsApiLoader;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;

class JobsApiLoaderTest extends TestCase
{

    /**
     * @throws GuzzleException
     */
    public function testLoadJobsTotalCountGreaterThanZero(): void
    {
        $apiClientMock = $this->createMock(ApiClient::class);
        $apiClientMock->expects($this->once())
            ->method('getJobsTotalCount')
            ->willReturn(10);

        $jobFactoryMock = $this->createMock(JobFactory::class);
        $salaryFactoryMock = $this->createMock(SalaryFactory::class);
        $contactFactoryMock = $this->createMock(ContactFactory::class);
        $addressFactoryMock = $this->createMock(AddressFactory::class);

        $jobsApiLoader = new JobsApiLoader(
            $apiClientMock,
            $jobFactoryMock,
            $salaryFactoryMock,
            $contactFactoryMock,
            $addressFactoryMock
        );

        $result = $jobsApiLoader->loadJobsTotalCount();

        $this->assertGreaterThan(0, $result);
    }
}
