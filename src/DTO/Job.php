<?php

namespace App\DTO;

use DateTime;

/**
 * Simpler representation of job data
 */
class Job
{
	private int $jobId;

	private string $title;

	private string $description;

	private DateTime $dateCreated;

	private ?Contact $contact;

	/**
	 * @var Address[]|null
	 */
	private ?array $addresses;

	private ?string $employment;

	private ?Salary $salary;

	private ?string $publicLink;

	public function __construct(
		int $jobId,
		string $title,
		string $description,
		DateTime $dateCreated,
		?string $publicLink,
		?string $employment,
		?Contact $contact = null,
		array $addresses = null,
		?Salary $salary = null,
	) {
		$this->jobId = $jobId;
		$this->title = $title;
		$this->description = $description;
		$this->dateCreated = $dateCreated;
		$this->publicLink = $publicLink;
		$this->employment = $employment;

		$this->contact = $contact;
		$this->addresses = $addresses;
		$this->salary = $salary;
	}

	public function getJobId(): int
	{
		return $this->jobId;
	}

	public function setJobId(int $jobId): void
	{
		$this->jobId = $jobId;
	}

	public function getTitle(): string
	{
		return $this->title;
	}

	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	public function getDescription(): string
	{
		return $this->description;
	}

	public function setDescription(string $description): void
	{
		$this->description = $description;
	}

	public function getDateCreated(): DateTime
	{
		return $this->dateCreated;
	}

	public function setDateCreated(DateTime $dateCreated): void
	{
		$this->dateCreated = $dateCreated;
	}

	public function getContact(): ?Contact
	{
		return $this->contact;
	}

	public function setContact(?Contact $contact): void
	{
		$this->contact = $contact;
	}

	/**
	 * @return Address[]|null
	 */
	public function getAddresses(): ?array
	{
		return $this->addresses;
	}

	/**
	 * @param Address[]|null $addresses
	 */
	public function setAddresses(?array $addresses): void
	{
		$this->addresses = $addresses;
	}

	public function getEmployment(): ?string
	{
	    return $this->employment;
	}

	public function setEmployment(?string $employment): void
	{
	    $this->employment = $employment;
	}

	public function getSalary(): ?Salary
	{
	    return $this->salary;
	}

	public function setSalary(?Salary $salary): void
	{
	    $this->salary = $salary;
	}

	public function getPublicLink(): ?string
	{
	    return $this->publicLink;
	}

	public function setPublicLink(?string $publicLink): void
	{
	    $this->publicLink = $publicLink;
	}

}