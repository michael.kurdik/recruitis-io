<?php

namespace App\DTO;

use DateTime;

/**
 * Simpler representation of salary data
 */
class Salary
{
    private bool $isRange;

    private ?float $min;

    private ?float $max;

    private string $currency;

    private string $unit;

    public function __construct(
        bool $isRange,
        ?float $min,
        ?float $max,
	    string $unit,
	    string $currency,
    ) {
        $this->isRange = $isRange;
        $this->min = $min;
        $this->max = $max;
	    $this->unit = $unit;
	    $this->currency = $currency;
    }

    public function getIsRange(): bool
    {
        return $this->isRange;
    }

    public function setIsRange(bool $isRange): void
    {
        $this->isRange = $isRange;
    }

    public function getMin(): ?float
    {
        return $this->min;
    }

    public function setMin(?float $min): void
    {
        $this->min = $min;
    }

    public function getMax(): ?float
    {
        return $this->max;
    }

    public function setMax(?float $max): void
    {
        $this->max = $max;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    public function getUnit(): string
    {
        return $this->unit;
    }

    public function setUnit(string $unit): void
    {
        $this->unit = $unit;
    }
}
