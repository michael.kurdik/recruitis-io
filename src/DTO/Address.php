<?php

namespace App\DTO;

/**
 * Simpler representation of address data
 */
class Address
{
    private string $city;

    private string $postcode;

    private string $street;

    private string $region;

    private string $state;

    public function __construct(
        string $city,
        string $postcode,
        string $street,
        string $region,
        string $state,
    ) {
        $this->city = $city;
        $this->postcode = $postcode;
        $this->street = $street;
        $this->region = $region;
        $this->state = $state;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function getPostcode(): string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): void
    {
        $this->postcode = $postcode;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    public function getRegion(): string
    {
        return $this->region;
    }

    public function setRegion(string $region): void
    {
        $this->region = $region;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): void
    {
        $this->state = $state;
    }
}
