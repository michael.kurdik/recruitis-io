<?php

namespace App\Util;

class Validators
{

	public static function isValidCurrency(string $currency): bool
	{
		return in_array($currency, [
			'EUR',
			'CZK',
			'USD'
		]);
	}

}