<?php declare(strict_types=1);

namespace App\Service;

use App\DTO\Job;
use App\Exceptions\RecruitisApiException;
use App\Factory\AddressFactory;
use App\Factory\ContactFactory;
use App\Factory\JobFactory;
use App\Factory\SalaryFactory;
use App\Service\Interface\IJobsLoader;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Implementation of loading data from API source
 */
class JobsApiLoader implements IJobsLoader
{

	public function __construct(
		protected readonly ApiClient $apiClient,
		protected readonly JobFactory $jobFactory,
		protected readonly SalaryFactory $salaryFactory,
		protected readonly ContactFactory $contactFactory,
		protected readonly AddressFactory $addressFactory,
	)
    {
    }

	/**
	 * @param int $page
	 * @return array|Job[]
	 * @throws GuzzleException|RecruitisApiException
	 */
    public function loadJobs(int $page, int $limit): array
    {
		$jobsPayload = $this->apiClient->getJobs($page, $limit);

	    $jobs = [];

	    foreach ($jobsPayload as $jobPayload) {
		    try {

			    $contact = null;
				if (($jobPayload['contact']) ?? null) {
					$contact = $this->contactFactory->createFromPayload($jobPayload['contact']);
				}

			    $salary = null;
				if (($jobPayload['salary']) ?? null) {
					$salary = $this->salaryFactory->createFromPayload($jobPayload['salary']);
				}

			    $addresses = [];
				if (($jobPayload['addresses']) ?? null) {
					foreach ($jobPayload['addresses'] as $addressPayload) {
						$addresses[] = $this->addressFactory->createFromPayload($addressPayload);
					}
				}

		        $job = $this->jobFactory->createFromPayload($jobPayload);

				$job->setContact($contact);
				$job->setSalary($salary);
				$job->setAddresses($addresses);

			    $jobs[] = $job;

		    } catch (\InvalidArgumentException $e) {
				// todo: log?
			    continue;
		    }
		}

	    return $jobs;
    }

	/**
	 * @throws GuzzleException
	 * @throws RecruitisApiException
	 */
	public function loadJobsTotalCount(): int
	{
		return $this->apiClient->getJobsTotalCount();
	}
}
