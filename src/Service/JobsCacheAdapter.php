<?php declare(strict_types=1);

namespace App\Service;

use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class JobsCacheAdapter
{
    private $cache;

    public function __construct(AdapterInterface $cache)
    {
        $this->cache = $cache;
    }

	/**
	 * @throws InvalidArgumentException
	 */
	public function loadForPage(int $page): array
    {
        $cacheItem = $this->cache->getItem((string) $page);

        if ($cacheItem->isHit()) {
            return $cacheItem->get();
        }

        return [];
    }

	/**
	 * @throws InvalidArgumentException
	 */
	public function saveForPage(int $page, array $jobs, int $expiration = 0): void
    {
        $pageJobs = $this->cache->getItem((string) $page);

        $pageJobs = $pageJobs->set($jobs);

        if ($expiration > 0) {
            $pageJobs->expiresAfter($expiration);
        }
        $this->cache->save($pageJobs);
    }
}

