<?php declare(strict_types=1);

namespace App\Service\Interface;

use App\DTO\Job;

/**
 * Interface for implementing of loading data from different sources
 */
interface IJobsLoader
{

	/**
	 * @param int $page
	 * @return array|Job[]
	 */
	public function loadJobs(int $page, int $limit): array;
	
}