<?php declare(strict_types=1);

namespace App\Service;

use App\Exceptions\RecruitisApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;

/**
 * Simple layer for process API jobs data
 */
class ApiClient
{

	protected Client $client;

	public function __construct(
		protected readonly string $apiUrl,
		protected readonly string $token,
	)
    {
        $this->client = new Client([
			'base_uri' => $apiUrl,
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
	            'Content-Type' => 'application/json',
            ],
       ]);
    }

	private function loadJobs(int $page, int $limit): array
	{
		// fixme: the url has to be complete even is defined in constructor
		$response = $this->client->requestAsync('GET', sprintf('%s/jobs', $this->apiUrl), [
			'query' => [
	            'page' => $page,
	            'limit' => $limit,
			]
        ])->wait();

        $statusCode = $response->getStatusCode();

        if ($statusCode !== 200) {
			throw new RecruitisApiException('Invalid status code');
        }

        $content = $response->getBody()->getContents();
		return json_decode($content, true);
	}

	/**
	 * @param $page
	 * @throws GuzzleException|RecruitisApiException
	 */
    public function getJobs(int $page, int $limit): array
    {
		// testing purpose
	    //$content = file_get_contents(__DIR__ . '/../../var/cache/jobs.json');
		//$data = json_decode($content, true);
	    //return $data['payload'];

	    $data = $this->loadJobs($page, $limit);

	    return $data['payload'];
    }

	/**
	 * @throws GuzzleException|RecruitisApiException
	 */
    public function getJobsTotalCount(): int
    {
		// testing purpose
	    //return 62;

	    $data = $this->loadJobs(1, 1);
	    $jobsTotalCount = $data['meta']['entries_total'] ?? 0;

	    return max($jobsTotalCount, 0);
    }
}
