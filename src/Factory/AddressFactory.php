<?php

namespace App\Factory;

use App\DTO\Address;
use InvalidArgumentException;

class AddressFactory
{

	public function createFromPayload(array $payload): Address
    {
		if (!($payload['city'] ?? null)) {
		    throw new InvalidArgumentException('Invalid argument city');
		}

		if (!($payload['postcode'] ?? null)) {
		    throw new InvalidArgumentException('Invalid argument postcode');
		}

		if (!($payload['street'] ?? null)) {
		    throw new InvalidArgumentException('Invalid argument street');
		}

		if (!($payload['region'] ?? null)) {
		    throw new InvalidArgumentException('Invalid argument region');
		}

		if (!($payload['state'] ?? null)) {
		    throw new InvalidArgumentException('Invalid argument state');
		}


		return new Address(
			$payload['city'],
			$payload['postcode'],
			$payload['street'],
			$payload['region'],
			$payload['state'],
	    );
    }

}
