<?php

namespace App\Factory;

use App\DTO\Job;
use InvalidArgumentException;

class JobFactory
{
    public function createFromPayload(array $payload): Job
    {
		if (!($payload['job_id'] ?? null) || $payload['job_id'] <= 0) {
			throw new InvalidArgumentException('Invalid argument job_id');
		}

		if (!($payload['title'] ?? null)) {
			throw new InvalidArgumentException('Invalid argument title');
		}

		if (!($payload['description'] ?? null)) {
			throw new InvalidArgumentException('Invalid argument description');
		}

		if (!($payload['date_created'] ?? null) || !($dateCreated = new \DateTime($payload['date_created']))) {
			throw new InvalidArgumentException('Invalid argument date_created');
		}

	    return new Job(
	        (int) $payload['job_id'],
            $payload['title'],
            $payload['description'],
            $dateCreated,
	        $payload['public_link'] ?? null,
	        $payload['employment']['name'] ?? null,
        );
    }


}
