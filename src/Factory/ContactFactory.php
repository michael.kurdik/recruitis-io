<?php

namespace App\Factory;

use App\DTO\Contact;
use InvalidArgumentException;

class ContactFactory
{
    public function createFromPayload(array $payload): Contact
    {

		// todo: validator, etc.
		if (!($payload['email'] ?? null) && !($payload['phone'] ?? null)) {
			throw new InvalidArgumentException('Invalid contact arguments');
		}

	    return new Contact(
			$data['contact']['name'] ?? null,
			$data['contact']['email'] ?? null,
			$data['contact']['phone'] ?? null
	    );
    }

}
