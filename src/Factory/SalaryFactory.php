<?php

namespace App\Factory;

use App\DTO\Salary;
use InvalidArgumentException;

class SalaryFactory
{
	public function __construct(
		protected readonly array $currencies,
		protected readonly array $salaryUnits,
	)
	{
	}

	public function createFromPayload(array $payload): Salary
    {

		if (!($payload['min'] ?? null) && !($payload['max'] ?? null)) {
			throw new InvalidArgumentException('Invalid argument salary value');
		}

		if (!($payload['currency'] ?? null) || !in_array($payload['currency'], $this->currencies)) {
			throw new InvalidArgumentException('Invalid argument salary currency');
		}

		if (!($payload['unit'] ?? null) || !in_array($payload['unit'], $this->salaryUnits)) {
			throw new InvalidArgumentException('Invalid argument salary unit');
		}

	    $isRange = $payload['is_range'] ?? null;
		if (!$isRange) {
			$isRange = isset($payload['min'], $payload['max']);
		}

	    $min = $payload['min'] ?? null;
		if ($min) {
			$min = (float) $min;
		}

	    $max = $payload['max'] ?? null;
		if ($max) {
			$max = (float) $max;
		}

		return new Salary(
			$isRange,
			$min,
			$max,
			$payload['currency'],
			$payload['unit']
		);
    }

}
