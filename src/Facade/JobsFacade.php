<?php

namespace App\Facade;

use App\DTO\Job;
use App\Service\Interface\IJobsLoader;
use App\Service\JobsCacheAdapter;
use Psr\Log\LoggerInterface;

class JobsFacade
{

	public function __construct(
		private readonly IJobsLoader $jobsLoader,
		private readonly JobsCacheAdapter $jobsCacheAdapter,
		private readonly LoggerInterface $logger,
	)
	{
	}

	/**
	 * @param int $page
	 * @param int $limit
	 * @param bool $cache
	 * @return array|Job[]
	 */
	public function getJobs(int $page, int $limit, bool $cache = false): array
	{
		$jobs = [];

		try {
			if ($cache) {
				$jobs = $this->jobsCacheAdapter->loadForPage($page);
			}

			if (!$cache || empty($jobs)) {
				$jobs = $this->jobsLoader->loadJobs($page, $limit);
			}

			if ($cache) {
				$this->jobsCacheAdapter->saveForPage($page, $jobs);
			}

		} catch (\Throwable $e) {
			$this->logger->error('Jobs loading failed.', ['exception' => $e]);
		}

		return $jobs;
	}


	/**
	 * @return int
	 */
	public function getJobsTotalCount(): int
	{
		try {

			return $this->jobsLoader->loadJobsTotalCount();

		} catch (\Throwable $e) {
			$this->logger->error('Jobs total count loading failed.', ['exception' => $e]);
		}

		return 0;
	}

}