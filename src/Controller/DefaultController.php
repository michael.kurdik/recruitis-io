<?php

namespace App\Controller;

use App\Facade\JobsFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
	public function __construct(private readonly JobsFacade $jobsFacade)
	{
	}

	#[Route('/', name: 'app_default')]
	#[Route('/{page}', name: 'app_default_page', requirements:['page' => '\d+'])]
	public function index(int $page = 1): Response
    {
		$jobsPerPage = (int)  $_ENV['JOBS_PER_PAGE'] ?? 1;
	    $total = $this->jobsFacade->getJobsTotalCount();
	    $totalPages = ceil($total / $jobsPerPage);

	    if ($totalPages && $page > $totalPages) {
		    return $this->redirectToRoute('app_default');
	    }

	    $jobs = $this->jobsFacade->getJobs($page, $jobsPerPage, true);

	    return $this->render('default.html.twig', [
            'jobs' => $jobs,
            'page' => $page,
            'total' => $total,
            'totalPages' => $totalPages,
            'jobsPerPage' => $jobsPerPage,
        ]);
    }
}
